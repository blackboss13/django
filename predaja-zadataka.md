# Procedura predaje zadataka na pismenom ispitu

Nakon što su vam nastavnici pregledali zadatke, možete ih postaviti na GitLab.

1. Prijavite se na svoj korisnički račun na GitLabu.
2. U gornjem desnom uglu kliknite na `+` i odaberite `New project`.
2. Odaberite `Create blank project`.
3. Unesite ime projekta u formatu `ime.prezime.2223-2` i odaberite `Private` za vidljivost.
3. Za opciju `Project deployment target` odaberite `No deployment planned`.
4. Isključite opciju `Initialize repository with a README` i kliknite na `Create project`.
5. Kada je projekt kreiran, pod opcijom `Project information` kliknite na `Members`.
6. U gornjem desnom uglu kliknite na `Invite members`.
7. Dodajte account `hleventic` kao `Owner`. Ponudit će vam  `hleventic` i `hleventic-ferit` accounte, dodajete `hleventic`.
8. U terminalu dodajte novi remote za svoj projekt:
   ```bash
   git remote add ispit URL_VAŠEG_NOVOG_REPOZITORIJA
   ```
9. Pushajte svoj kod na GitLab:
   ```bash
   git push ispit master
   ```
